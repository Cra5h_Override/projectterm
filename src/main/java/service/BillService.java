/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dao.BillDao;
import model.Bill;

/**
 *
 * @author Zerocool
 */
public class BillService {

    private static Bill bill;
    private static double cash;

    public static void loadBill(Bill newBill,double newCash) {
        bill = newBill;
        cash = newCash;
    }

    public static void loadBillTest() {
        BillDao dao = new BillDao();
        bill = dao.get(4);
        cash = 500;
    }

    public static Bill getBill() {
        return bill;
    }

    public static double getCash() {
        return cash;
    }
 
    public static double getBalance() {
        return cash-bill.getTotal();
    }

}
