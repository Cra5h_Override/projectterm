/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.Product;

/**
 *
 * @author Zerocool
 */
public class BillAndProduct {
    private int id;
    private Product product;
    private int amount;
    private double price;
    private Bill bill;

    public BillAndProduct(int id, Product product, int amount,double price, Bill bill) {
        this.id = id;
        this.product = product;
        this.amount = amount;
        this.bill = bill;
        this.price =price;
    }

    public BillAndProduct(Product product, int amount,double price, Bill bill) {
        this(-1,product,amount,price,bill);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }
    public void addAmount(int amount) {
        this.amount = this.amount+amount;
    }
    
    public double getTotal(){
        return amount *price;
    }

    @Override
    public String toString() {
        return "BillAndProduct{" + "id=" + id 
                + ", product=" + product 
                + ", amount=" + amount 
                + ", price=" + price 
                + ", total=" + this.getTotal()
                + "}";
    }
    
}
