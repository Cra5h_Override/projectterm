/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author 59161111
 */
public class Stock {

    private int id;
    private String name;
    private int qty;
    private String unit;
    private double buy_price;

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", name=" + name + ", qty=" + qty + ", unit=" + unit + ", buy_price=" + buy_price + '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setBuy_price(double buy_price) {
        this.buy_price = buy_price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getQty() {
        return qty;
    }

    public String getUnit() {
        return unit;
    }

    public double getBuy_price() {
        return buy_price;
    }
    public Stock(int id, String name, int qty, String unit, double buy_price) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.unit = unit;
        this.buy_price = buy_price;
    }

}
