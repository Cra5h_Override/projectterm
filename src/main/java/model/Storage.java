/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author 59161111
 */
public class Storage {
    private int id;
    private Stock stock;
    private User user;
    private int qty;
    private String unit;
    private Date created;
    private int type;


    public Storage(int id, Stock stock, User user, int qty, String unit, Date created, int type) {
        this.id = id;
        this.stock = stock;
        this.user = user;
        this.qty = qty;
        this.unit = unit;
        this.created = created;
        this.type = type;
    }
    public Storage(Stock stock, User user, int qty, String unit, int type) {
        this(-1,stock,user,qty,unit,null,type);
    }


    
    public int getId() {
        return id;
    }
    


    public Stock getStock() {
        return stock;
    }

    public User getUser() {
        return user;
    }

    public int getQty() {
        return qty;
    }

    public String getUnit() {
        return unit;
    }

    public Date getCreated() {
        return created;
    }

    public int getType() {
        return type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        String str = "Storage{" + "id=" + id 
                + ", stock=" + stock 
                + ", user=" + user 
                + ", qty=" + qty 
                + ", unit=" + unit 
                + ", created=" + created 
                + ", type=" + type 
                + '}';
        return str;
    }
}
