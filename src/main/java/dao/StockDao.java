/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Stock;

/**
 *
 * @author 59161111
 */
public class StockDao implements DaoInterface<Stock> {

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO stock (name, qty, unit, buy_price) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getQty());
            stmt.setString(3, object.getUnit());
            stmt.setDouble(4, object.getBuy_price());
           

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Cann't add product");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,qty,unit,buy_price FROM stock;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                int qty = result.getInt("qty");
                String unit = result.getString("unit");
                double buy_price = result.getDouble("buy_price");
                Stock stock = new Stock(id,name,qty,unit,buy_price);
                list.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Cann't get all Stock");
        }
        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
         try {
            String sql = "SELECT id,name,qty,unit,buy_price FROM stock WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int pid = result.getInt("id");
                String name = result.getString("name");
                int qty = result.getInt("qty");
                String unit = result.getString("unit");
                double buy_price = result.getDouble("buy_price");
                
                
                Stock stock = new Stock(pid,name,qty,unit,buy_price);
                return stock;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Cann't get  stock");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM stock WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Cann't delete  Stock");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE stock SET name = ?,qty = ?,unit = ?,buy_price = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getQty());
            stmt.setString(3, object.getUnit());
            stmt.setDouble(4, object.getBuy_price());
         
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Cann't update product");
        }
        db.close();
        return row;  }

    public static void main(String[] args) {
      
    }

   
}
