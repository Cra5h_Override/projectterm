/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Stock;
import model.Storage;
import model.User;

/**
 *
 * @author 59161111
 */
public class StorageDao implements DaoInterface<Storage> {

    @Override
    public int add(Storage object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO storage (stock_id,user_id,stock_qty,stock_unit,type) VALUES (?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getStock().getId());
            stmt.setInt(2, object.getUser().getId());
            stmt.setInt(3, object.getQty());
            stmt.setString(4, object.getUnit());
            stmt.setInt(5, object.getType());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);

            }
            //add
            if (object.getType() == 0) {
//                    System.out.println("update stock");
                String sqlStock = "UPDATE stock SET qty = ? WHERE id = ?;";
                PreparedStatement stmtStock = conn.prepareStatement(sqlStock);
                stmtStock.setInt(1, object.getStock().getQty() + object.getQty());
                stmtStock.setInt(2, object.getStock().getId());
                stmtStock.executeUpdate();
            }
            //del
            if (object.getType() == 1) {
                String sqlStock = "UPDATE stock SET qty = ? WHERE id = ?;";
                PreparedStatement stmtStock = conn.prepareStatement(sqlStock);
                stmtStock.setInt(1, object.getStock().getQty() - object.getQty());
                stmtStock.setInt(2, object.getStock().getId());
                stmtStock.executeUpdate();
            }
        } catch (SQLException ex) {
            System.out.println("Error: don't add Storage" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Storage> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT str.id as id,stock_id,s.name as stock_name,user_id,u.username as username,stock_qty,stock_unit,created,str.type as type FROM storage str , stock s ,user u WHERE str.stock_id = s.id AND str.user_id = u.id;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int stock_id = result.getInt("stock_id");
                String stock_name = result.getString("stock_name");
                int user_id = result.getInt("user_id");
                String user_username = result.getString("username");
                int stock_qty = result.getInt("stock_qty");
                String stock_unit = result.getString("stock_unit");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int type = result.getInt("type");
                Storage storage = new Storage(id, new Stock(stock_id, stock_name, 0, "", 0), new User(user_id, user_username, ""), stock_qty, stock_unit, created, type);
                list.add(storage);
            }
        } catch (SQLException ex) {
            System.out.println("Error: don't get all Storage" + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(StorageDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Storage get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM storage WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error: Cann't delete  Storage");
        }
        db.close();
        return row;
    }

    @Override
    public int update(Storage object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE storage SET stock_qty = ? , type = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);


            stmt.setInt(1, object.getQty());
            stmt.setInt(2, object.getType());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
  

        } catch (SQLException ex) {
            System.out.println("Error: Cann't update Storage");
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
    }
}
